package state;

import state.car.*;

public class CarContext {
    private int currentGearStateIndex = 0;
    private int currentSpeed     = 0;
    private final Gear[] GEARS_TABLE = { new Park(), new Gear1(), new Gear2(), new Gear3(), new Gear4(), new Gear5() };
    
    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public int changeSpeed(int speed){
      this.currentSpeed = speed;
      GEARS_TABLE[currentGearStateIndex].changeSpeed(this);
      return currentGearStateIndex;
    }

    //used by Gear
    public void changeCurrentGearState(int gear){
        currentGearStateIndex = gear;
        changeSpeed(currentSpeed);
    }
}
