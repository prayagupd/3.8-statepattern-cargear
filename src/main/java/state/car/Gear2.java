package state.car;

import state.CarContext;

public class Gear2 extends AbstractGear {

    public Gear2(){
        super(5, 10);
        setPreviousState(1);
        setNextState(3);
    }
}
