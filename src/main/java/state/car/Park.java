package state.car;

import state.CarContext;

public class Park extends AbstractGear {
    public Park() {
        super(0, 0);
        setPreviousState(0);
        setNextState(1);
    }

    public void changeSpeed(CarContext carContext){
        if (carContext.getCurrentSpeed() != 0) {
            carContext.changeCurrentGearState(nextState());
        }
    }
}
