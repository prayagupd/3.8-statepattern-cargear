package state.car;

import state.CarContext;

public class Gear1 extends AbstractGear {
    private final int state = 1;

    public Gear1(){
        super(0, 5);
        setPreviousState(0);
        setNextState(2);
    }

    public void changeSpeed(CarContext carContext){
        if (carContext.getCurrentSpeed() == minSpeed()) {
          carContext.changeCurrentGearState(previousState());
        } else if (carContext.getCurrentSpeed() > maxSpeed()) {
          carContext.changeCurrentGearState(nextState());
        }
    }
}
