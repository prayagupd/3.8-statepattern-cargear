package state.car;

import state.CarContext;

public class Gear3 extends AbstractGear {

    public Gear3(){
        super(10, 30);
        setPreviousState(2);
        setNextState(4);
    }
}
