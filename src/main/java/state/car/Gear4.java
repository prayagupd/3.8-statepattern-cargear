package state.car;

import state.CarContext;

public class Gear4 extends AbstractGear {

    public Gear4(){
        super(30, 55);
        setPreviousState(3);
        setNextState(5);
    }
}
