package state.car;

import state.CarContext;

/**
 * Created by 984493
 * on 4/9/2015.
 */

public abstract class AbstractGear implements Gear {

    private int minSpeed;
    private int maxSpeed;

    private int previousState;
    private int nextState;

    AbstractGear( final int minSpeed, final int maxSpeed){
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
    }

    public int minSpeed() {
        return minSpeed;
    }

    public void setMinSpeed(int minSpeed) {
        this.minSpeed = minSpeed;
    }

    public int maxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int previousState() {
        return previousState;
    }

    public void setPreviousState(int previousState) {
        this.previousState = previousState;
    }

    public int nextState() {
        return nextState;
    }

    public void setNextState(int nextState) {
        this.nextState = nextState;
    }

    public void changeSpeed(CarContext carContext){
        if (carContext.getCurrentSpeed() < minSpeed()) {
            carContext.changeCurrentGearState(previousState());
        } else if (carContext.getCurrentSpeed() > maxSpeed()) {
            carContext.changeCurrentGearState(nextState());
        }
    }

}
