package state.car;

import state.CarContext;

public interface Gear {

    public void changeSpeed(CarContext carContext);
}