package state.car;

import state.CarContext;

public class Gear5 extends AbstractGear {

    public Gear5(){
        super(0, 55);
        setPreviousState(4);
        setNextState(0);
    }

    public void changeSpeed(CarContext carContext){
        if (carContext.getCurrentSpeed() < maxSpeed())
            carContext.changeCurrentGearState(previousState());
    }
}
